# md2pdf - markdown to pdf converter

[![Please don't upload to GitHub](https://nogithub.codeberg.page/badge.svg)](https://nogithub.codeberg.page)

Markdown to PDF conversion utilizing ```pandoc``` which has support for CSS styling.

## Installation

1. Clone the repository

```sh
git clone https://codeberg.org/stoerdebegga/md2pdf.git
```

2. Install binary and assets

```sh
make install
```

## Usage

```sh
mmd2pdf - markdown to pdf converter

Usage:

  /home/sascha/.local/bin/md2pdf [options]

Options

  -h: display help information
  -i: input markdown file
  -o: output pdf file
  -s: style, resides in ${HOME}/.local/share/md2pdf/styles/
  -t: title of pdf
```

## Author

stoerdebegga <https://codeberg.org/stoerdebegga>

## License

This software is released under the MIT License, see LICENSE.
