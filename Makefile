BINDIR=$(HOME)/.local/bin
STYLESDIR=$(HOME)/.local/share/md2pdf/styles

install:
	@echo "installing md2pdf"
	@mkdir -p $(BINDIR)
	@cp md2pdf $(BINDIR)/md2pdf
	@chmod u+x $(BINDIR)/md2pdf
	@echo "installing styles"
	@mkdir -p $(STYLESDIR)
	@cp -r styles/* $(STYLESDIR)

uninstall:
	@echo "uninstalling md2pdf"
	@rm -f $(BINDIR)/md2pdf
	@echo "uninstalling styles"
	@rm -rf $(STYLESDIR)
